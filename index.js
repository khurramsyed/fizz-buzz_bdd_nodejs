var http = require('http');
var FizzBuzz = require('./app/FizzBuzz')


var express = require('express');
var app = express();

app.get('/:id', function (req, res) {
	var fizzBuzz = new FizzBuzz();
	var responseText = fizzBuzz.getFizzBuzz(req.params.id);
	res.end(responseText);
});

var server = app.listen(8081, function () {

   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)

});
