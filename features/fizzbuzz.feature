Feature: Fizzbuzz API

	Scenario: Requesting fizzbuzz for a number
		Given there is a fizzbuzz service running
		When I make a request to the fizzbuzz service for the number 3
		Then I should see the response as 'Fizz'

	Scenario: Requesting fizzbuzz for a number
                Given there is a fizzbuzz service running
                When I make a request to the fizzbuzz service for the number 5
                Then I should see the response as 'Buzz'

	Scenario: Requesting fizzbuzz for a number
                Given there is a fizzbuzz service running
                When I make a request to the fizzbuzz service for the number 0
                Then I should see the response as '0'	

	Scenario: Requesting fizzbuzz for a number
                Given there is a fizzbuzz service running
                When I make a request to the fizzbuzz service for the number 15
                Then I should see the response as 'FizzBuzz'
