// features/step_definitions/browser_steps.js
var chai = require('chai')
var expect = chai.expect;
var {defineSupportCode} = require('cucumber');
var request = require('request-promise');


defineSupportCode(function({Given, When, Then}) {
  	
	Given('there is a fizzbuzz service running', function () {
		
	});

	When('I make a request to the fizzbuzz service for the number {num}', function (num, callback) {
		this.result = request('http://localhost:8081/'+num);	
		callback();
	});

	Then('I should see the response as \'{string}\'', function(string, callback) {
		this.result.then(function(response){
			expect(response).to.equal(string);
			callback();
		});
	 });

});
