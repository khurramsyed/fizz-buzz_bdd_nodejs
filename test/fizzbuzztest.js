var chai = require('chai');
var expect = chai.expect; 
var FizzBuzz = require("./../app/FizzBuzz")

describe("FizzBuzz", function() {
	describe("for numbers not divisible by 3 and 5", function(){
		it('is the nu.equalr itself', function() {
		    var fizzBuzz = new FizzBuzz();
		    expect(fizzBuzz.getFizzBuzz(1)).to.equal(1);
		    expect(fizzBuzz.getFizzBuzz(2)).to.equal(2);
		    expect(fizzBuzz.getFizzBuzz(7)).to.equal(7);
		});
	});

	describe("for numbers divisible by 3", function(){
		it('is Fizz', function() {
		    var fizzBuzz = new FizzBuzz();
		    expect(fizzBuzz.getFizzBuzz(3)).to.equal("Fizz");
		    expect(fizzBuzz.getFizzBuzz(6)).to.equal("Fizz");
		    expect(fizzBuzz.getFizzBuzz(27)).to.equal("Fizz");
		});
	});


	describe("for numbers divisible by 5", function(){
		it('is Buzz ', function() {
		    var fizzBuzz = new FizzBuzz();
		    expect(fizzBuzz.getFizzBuzz(5)).to.equal("Buzz");
		    expect(fizzBuzz.getFizzBuzz(10)).to.equal("Buzz");
		    expect(fizzBuzz.getFizzBuzz(20)).to.equal("Buzz");
		});
	});

	describe("for numbers divisible by 3 and 5", function(){
		it('is FizzBuzz ', function() {
		    var fizzBuzz = new FizzBuzz();
		    expect(fizzBuzz.getFizzBuzz(15)).to.equal("FizzBuzz");
		    expect(fizzBuzz.getFizzBuzz(30)).to.equal("FizzBuzz");
		    expect(fizzBuzz.getFizzBuzz(45)).to.equal("FizzBuzz");
		});
	});

	describe("for zero", function(){
		it('is neither fizz nor buzz', function(){
			var fizzBuzz = new FizzBuzz();
			expect(fizzBuzz.getFizzBuzz(0)).to.equal(0);
		});

	});
});
