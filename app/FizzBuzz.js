function FizzBuzz() {
}

FizzBuzz.prototype.getFizzBuzz = function(theNumber){
	if(isDivisibleBy(theNumber,15))
		return "FizzBuzz";
	if(isDivisibleBy(theNumber, 3))
		return "Fizz";
	if(isDivisibleBy(theNumber,5))
		return "Buzz";
	return theNumber;
}

FizzBuzz.prototype.divisibleBy = function(theNumber, theDivisor){
	return isDivisibleBy(theNumber, theDivisor); 
}


function isDivisibleBy(theNumber, theDivisor){
	return (theNumber != 0 && theNumber % theDivisor == 0)
}

module.exports = FizzBuzz;
